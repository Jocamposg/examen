import { identifierModuleUrl } from '@angular/compiler';
import { applySourceSpanToExpressionIfNeeded } from '@angular/compiler/src/output/output_ast';
import { ɵangular_packages_platform_browser_dynamic_platform_browser_dynamic_a } from '@angular/platform-browser-dynamic';
import gql from 'graphql-tag';

export const CREATE_INCIDENT = gql`
mutation ($incident: IncidentInput!){
  createIncident(input: $incident){
    id
    description
    iDate
    fDate
  }
}
`;

export const UPDATE_INCIDENT = gql`
mutation ($incident: IncidentInput!){
  createIncident(input: $incident){
    id
    description
    iDate
    fDate
  }
}
`;


export const DELETE_INCIDENT = gql`
mutation($id: ID!) {
    deleteIncident(id: $id){
      id
    }
  }
`;