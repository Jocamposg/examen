import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { INCIDENT_QUERY } from './queries';
import { CREATE_INCIDENT, DELETE_INCIDENT } from './mutation';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { CalendarOptions, DateSelectArg, EventApi, EventClickArg, filterEventStoreDefs } from '@fullcalendar/angular'; // useful for typechecking


@Component({
  selector: 'app-incident-graphql',
  templateUrl: './incident-graphql.component.html',
  styleUrls: ['./incident-graphql.component.css']
})
export class IncidentGraphqlComponent implements OnInit {

  public incidents: any;
  public currentIncident: any;
  public isFormVisible = false;
  public description = null;
  public iDate = null;
  public fDate = null;
  constructor(private apollo: Apollo) { 
    this.incidents = [];
    
  }

  public events= [
    { title: 'event 1', date: '04-04-2021' },
    { title: 'event 2', date: '2021-04-02' }
  ]
  

  ngOnInit(): void {
    this.calendarOptions,
    this.filter();
    
  }

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    },
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    initialEvents: this.events,
    //initialEvents: this.filter,
    select: this.handleDateSelect.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    
    
  };
  
  currentEvents: EventApi[] = [];

  handleWeekendsToggle() {
    const { calendarOptions } = this;
    calendarOptions.weekends = !calendarOptions.weekends;
  }

  handleDateSelect(selectInfo: DateSelectArg) {
    const title = prompt('Please enter a new title for your event');
    const calendarApi = selectInfo.view.calendar;

    calendarApi.unselect(); // clear date selection

    if (title) {
      calendarApi.addEvent({
        id: createEventId(),
        title,
        start: selectInfo.startStr,
        end: selectInfo.endStr,
        allDay: selectInfo.allDay
      });
    }
  }

  handleEventClick(clickInfo: EventClickArg) {
    if (confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
      clickInfo.event.remove();
    }
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
  }

  reset(){
    this.currentIncident = {
      description: '',
      iDate: '',
      fDate:''
    };
    this.isFormVisible = false;
  }

  filter() {
    this.apollo.watchQuery({
      query: INCIDENT_QUERY,
      fetchPolicy: 'network-only',
      variables: {
        description: this.description === '' ? null : this.description,
        iDate: this.iDate === '' ? null : this.iDate
      }
    }).valueChanges.subscribe(result => {
      this.incidents = result.data;
      this.reset();
    });
  }

  delete(id: number){
    this.apollo.mutate({
      mutation: DELETE_INCIDENT,
      variables: {
        id: id
      }
    }).subscribe(() => {
      this.filter();
    });
  }

  save() {
    let incident = {
      description: this.currentIncident.description,
      iDate: this.currentIncident.iDate,
      fDate: this.currentIncident.fDate
    };
    this.apollo.mutate({
      mutation: CREATE_INCIDENT,
      variables: {
        incident: incident
      }
    }).subscribe(() => {
      this.filter();
    });
  }

}

function createEventId(): string | undefined {
  throw new Error('Function not implemented.');
}

