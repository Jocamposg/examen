import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentGraphqlComponent } from './incident-graphql.component';

describe('IncidentGraphqlComponent', () => {
  let component: IncidentGraphqlComponent;
  let fixture: ComponentFixture<IncidentGraphqlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncidentGraphqlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentGraphqlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
