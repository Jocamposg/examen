import gql from 'graphql-tag';

export const INCIDENT_QUERY = gql`
query{
    incidents {
    description
    iDate
    }
}
`;