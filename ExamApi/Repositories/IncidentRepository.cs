using Microsoft.EntityFrameworkCore;
using ExamApi.Models;
using System.Collections.Generic;
using System.Linq;
using GraphQL.Types;

namespace ExamApi.Repositories
{
    public class IncidentRepository
    {
        private readonly DatabaseContext _context;
        public IncidentRepository(DatabaseContext context)
        {
            _context = context;
        }

        
        public IEnumerable<Incident> All(){
            return _context.Incidents.ToList();
        }

        /////
        public Incident Find(long id) {
            return _context.Incidents.Find(id);
        }

        public IEnumerable<Incident> Filter(ResolveFieldContext<object> graphqlContext){
            var results = from incidents in _context.Incidents select incidents;
            if (graphqlContext.HasArgument("description")) {
                var description = graphqlContext.GetArgument<string>("description");
                results = results.Where(c => c.Description.Contains(description));
            }
            return results;
        }

        ////
        public Incident Create(Incident incident){
            _context.Incidents.Add(incident);
            _context.SaveChanges();
            return incident;
        }

        ////
        public Incident Delete(long id){
            var incident = _context.Incidents.Find(id);
            if (incident == null) {
                return null;
            }
            _context.Incidents.Remove(incident);
            _context.SaveChanges();
            return incident;
        }

    }
}















