using GraphQL;
using GraphQL.Types;
using System.Linq;
using ExamApi.GraphQL.Types;
using ExamApi.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ExamApi.Repositories;

namespace ExamApi.GraphQL
{
    class ProjectMutation : ObjectGraphType
    {
        public ProjectMutation(IncidentRepository incidentRepository)
        {
            Field<IncidentType>("createIncident",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IncidentInputType>> { Name = "input" }),
                resolve: context => incidentRepository.Create(context.GetArgument<Incident>("input"))
            );
            

            Field<IncidentType>("deleteIncident",
                arguments: new QueryArguments(new QueryArgument<IdGraphType> { Name = "id" }),
                resolve: context => incidentRepository.Delete(context.GetArgument<long>("id"))
            ); 
        }
    }
}