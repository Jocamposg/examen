using GraphQL;
using GraphQL.Types;
using System.Linq;
using ExamApi.GraphQL.Types;
using ExamApi.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ExamApi.Repositories;

namespace ExamApi.GraphQL
{
    class ProjectQuery : ObjectGraphType
    {
        public ProjectQuery(IncidentRepository incidentRepository)
        {
            Field<ListGraphType<IncidentType>>("incidents",
                arguments: new QueryArguments(
                    new QueryArgument<StringGraphType> { Name = "description" },
                    new QueryArgument<DateGraphType> { Name = "iDate" },
                    new QueryArgument<DateGraphType> { Name = "fDate" }
                ),
                resolve: context => incidentRepository.Filter(context)
            );

            Field<ListGraphType<IncidentType>>("incidentsL", resolve: context => incidentRepository.All());
            
            Field<IncidentType>("incident", 
                arguments: new QueryArguments(new QueryArgument<IdGraphType> { Name = "id" }),
                resolve: context => incidentRepository.Find(context.GetArgument<long>("id"))
            );
        }
    }
}