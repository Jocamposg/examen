using GraphQL.Types;
using ExamApi.Models;

namespace ExamApi.GraphQL.Types
{
    public class IncidentInputType : InputObjectGraphType
    {
        public IncidentInputType()
        {
            Name = "IncidentInput";
            Field<NonNullGraphType<StringGraphType>>("description");
            Field<NonNullGraphType<DateGraphType>>("iDate");
            Field<NonNullGraphType<DateGraphType>>("fDate");
        }
    }
}