using GraphQL.Types;
using ExamApi.Models;
using ExamApi.Repositories;

namespace ExamApi.GraphQL.Types
{
    public class IncidentType : ObjectGraphType<Incident>
    {
        public IncidentType(IncidentRepository repository)
        {
            Name = "Incident";
            Field(x => x.Id);
            Field(x => x.Description);
            Field(x => x.IDate);
            Field(x => x.FDate);
        }
    }
}