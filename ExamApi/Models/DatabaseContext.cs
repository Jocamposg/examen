using Microsoft.EntityFrameworkCore;

namespace ExamApi.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            
        }

        public DbSet<Incident> Incidents { get; set; }
    }
}