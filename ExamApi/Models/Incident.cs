using System.Collections.Generic;
using System;

namespace ExamApi.Models
{
    public class Incident
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public DateTime IDate { get; set; }
        public DateTime FDate { get; set; }

    }
}